use std::collections::HashMap;
use std::time::Duration;
use std::fs;

use clap::{Arg, App};

fn main() {
    let matches = App::new("The Survey Scan app")
        .version("1.0")
        .author("Moundrin Pain")
        .about("Takes a scan as input, and tells you how much that belt is worth")
        .arg(Arg::with_name("mining_rate")
             .short("r")
             .value_name("rate")
             .help("How many m3 cycle is mined")
             .default_value("1015"))
        .arg(Arg::with_name("cycle_time")
             .short("c")
             .value_name("cycle_time")
             .help("How long each cycle takes")
             .default_value("180"))
        .arg(Arg::with_name("input")
             .short("f")
             .value_name("scan-file")
             .help("Input file")
             .default_value("survey_scan.txt"))
        .get_matches();
    let rate = matches.value_of("mining_rate").unwrap().parse::<f64>().unwrap();
    let cycle = matches.value_of("cycle_time").unwrap().parse::<f64>().unwrap();
    let filename = matches.value_of("input").unwrap();
    let mut values: HashMap<&str, f64> = HashMap::default();
    for (k, v) in vec!(("Veldspar", 317.4), ("Concentrated Veldspar", 290.1), ("Dense Veldspar", 301.4)) {
        values.insert(k, v);
    }
    let contents = fs::read_to_string(filename).unwrap();
    let values= contents
        .lines()
        .map(|line| -> (&str, f64) {
            let parts: Vec<&str> = line.split('\t').collect();
            (parts[0], parts[2].replace(",", "").trim_end_matches(" m3").parse().unwrap())
        })
        .filter(|(name, _)| values.contains_key(name))
        .map(|(name, quant)| -> (&str, f64, f64) {
            let val = values[name] * quant;
            (name, quant, val)
        })
        .collect::<Vec<(&str, f64, f64)>>().iter()
        .fold(HashMap::default(), |mut acc, &(name, quant, val)| -> HashMap<String, (f64, Duration)>{
            let old_value: (f64, Duration) = *acc.get(name).unwrap_or(&(0f64, Duration::from_secs(0)));
            let time = Duration::from_secs(((quant / rate) * cycle) as u64) + old_value.1;
            acc.insert(String::from(name), (val + old_value.0, time));
            acc
        });
    let mut total = 0f64;
    let mut total_duration = Duration::from_secs(0);
    for (k, (value, duration)) in values {
        println!("{} => {:.3} in {:.3} hours ({:.3} isk/hour)", k, value, duration.as_secs() as f64/60f64/60f64, value/(duration.as_secs() as f64/60f64/60f64));
        total += value;
        total_duration += duration;
    }
    println!("Total: {} in {:.3} hours ({:.3} isk/hour)", total, total_duration.as_secs() as f64/60f64/60f64, total/(total_duration.as_secs() as f64/60f64/60f64) );
}
