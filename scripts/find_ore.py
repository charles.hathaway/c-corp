import unittest
import argparse
import sys
import re
from datetime import datetime, timedelta
from os import listdir
from os.path import isfile, join
from pathlib import Path

time_format = "%Y-%m-%d %H:%M:%S"

parser = argparse.ArgumentParser(description="Extract mining logs from a log file")
parser.add_argument("--log-dir", dest="log_dir",  default=f"{str(Path.home())}/My Documents/EVE/logs/GameLogs")
parser.add_argument("--test", dest="test", action="store_true",  default=False, help="if set, included tests will be run")
parser.add_argument("--start", dest="start_time", type=lambda s: datetime.strptime(s, time_format),
    default=datetime.today() - timedelta(days=1),
    help=f"specifies the start time; default is yesterday. Format is {time_format.replace('%', '')}")
parser.add_argument("--end", dest="end_time", type=lambda s: datetime.strptime(s, time_format),
    default=datetime.today() + timedelta(days=1),
    help=f"specifies the end time; default is tomorrow. Format is {time_format.replace('%', '')}")

miningRegexp = re.compile("You mined (?P<count>\d+) units of (?P<name>[^\.\n]+)$")
formatRegexp = re.compile("<[^>]+>")

def parse_line(line: str) -> (int, str):
    res = miningRegexp.search(line)
    count = 0
    name = ""
    if res != None:
        count = int(res.group("count"))
        name = res.group("name")
    return (count, name)

def remove_formatting(line: str) -> str:
    line = formatRegexp.sub("", line)
    return line

def to_time(line: str) -> datetime:
    return datetime.strptime(line, '%Y%m%d_%H%M%S.txt')

def main(args):
    print("Got values: %s" % args.log_dir)
    start_time = args.start_time
    end_time = args.end_time
    files = [f for f in listdir(args.log_dir) if isfile(join(args.log_dir, f))]
    result = {}
    for f in files:
        cur_time = to_time(f)
        if cur_time < start_time or cur_time > end_time:
            continue
        print("Scanning: %s" % f)
        with open(join(args.log_dir, f), "r") as f:
            for line in f.readlines():
                line = remove_formatting(line)
                count, name = parse_line(line)
                if count > 0:
                    if name not in result:
                        result[name] = 0
                    result[name] += count
    print(result)

class TestParseLine(unittest.TestCase):
    def test_parse_line(self):
        test_cases = [
                {
                    "input": "[ 2020.04.10 18:57:01 ] (mining) You mined 362 units of Concentrated Veldspar",
                    "wantQuantity": 362,
                    "wantName": "Concentrated Veldspar",
                },
                {
                    "input": "[ 2020.04.10 18:49:25 ] (None) Undocking from Teonusude III - Moon 1 - Republic Fleet Assembly Plant to Teonusude solar system.",
                    "wantQuantity": 0,
                    "wantName": "",
                },
                {
                    "input": "",
                    "wantQuantity": 0,
                    "wantName": "",
                },
        ]
        for tc in test_cases:
            gotQuantity, gotName = parse_line(tc["input"])
            self.assertEqual(gotQuantity, tc["wantQuantity"])
            self.assertEqual(gotName, tc["wantName"])

    def test_remove_formatting(self):
        test_cases = [
                {
                    "input": "hello world",
                    "output": "hello world"
                },
                {
                    "input": "<format>hello world",
                    "output": "hello world"
                },
        ]
        for tc in test_cases:
            got = remove_formatting(tc["input"])
            self.assertEqual(got, tc["output"])

if __name__ == "__main__":
    args = parser.parse_args()
    if args.test:
        sys.argv = [sys.argv[0]] + sys.argv[2:]
        unittest.main()
    else:
        main(args)
