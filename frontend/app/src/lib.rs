use wasm_bindgen::prelude::*;
use yew::{html, Callback, Component, ComponentLink, Html, ShouldRender};

struct App { }

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        App {
        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="body">
                <header>
                    <div class="header-row">
                        <section class="header-section">
                            <span class="header-title">{"C-Corp"}</span>
                        </section>
                    </div>
                </header>
                <div class="ribon"></div>
                <div class="content">{"Hello world!"}</div>
            </div>
        }
    }
}

pub fn app_main() -> Result<(), JsValue> {
    yew::start_app::<App>();
    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
