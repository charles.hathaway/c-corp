/* random utility stuff */

// map is used to construct a map with the provided values
// the first element should be a new map, which implements the 'insert' operation,
// and the following arguments should be of the form 'key => value,'
#[macro_export]
macro_rules! map(
    { $m:expr $(, $key:expr => $value:expr)* ,} => {
        {
            let mut m = $m;
            $(
                m.insert($key, $value.into());
            )*
            m
        }
    };

    { $m:expr $(, $key:expr => $value:expr)*} => {
        {
            map!($m $(, $key => $value)*,)
        }
    };
);
