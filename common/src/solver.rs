/* solver.rs contains code to identify the best use of resources
 * and calculate a predicted value/hour of an operation
 *
 * Node represents an abstract 'process' which takes some inputs (including time)
 * and produces some outputs. These are meant to be concrete domain-specific instantiations,
 * such as an Amarr Shuttle blueprint and tritanium.
 *
 * We construct a problem from a set of Nodes (a mart of things we can transform
 * from and too), a target output (such as Isk), 
 */
use chrono::Duration;
use fnv::FnvHashMap as HashMap;
use std::convert::From;
use std::error::Error;
use std::fmt::Debug;
use std::fmt;

#[derive(Eq, PartialEq, Debug)]
pub enum SolverError {
    MalformedNode(String),
}

impl fmt::Display for SolverError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "encountered error during solving: {:?}", self)
    }
}

impl Error for SolverError {
}

pub trait NodeFactory: Debug {
    // list_options returns a list of nodes which produce the item in question
    fn list_options(&self, thing: &Resource) -> Vec<Node>;
}

/* Resource reprents an object; something used as input/output to a node
 */
#[derive(Hash, Eq, PartialEq, Debug)]
pub struct Resource {
    name: String,
}

impl Resource {
    pub fn time() -> Self {
        Resource::from("cycle time")
    }
}

impl From<&str> for Resource {
    fn from(name: &str) -> Self {
        Resource{name: name.to_string()}
    }
}

#[derive(Hash, Eq, PartialEq, Debug)]
pub enum ResourceValue {
    Int(usize),
    Time(Duration),
}

impl From<usize> for ResourceValue {
    fn from(val: usize) -> Self {
        ResourceValue::Int(val)
    }
}

impl From<Duration> for ResourceValue {
    fn from(val: Duration) -> Self {
        ResourceValue::Time(val)
    }
}

/* Node represents a generic process which takes inputs and produces outputs in a given time
 */
#[derive(Eq, PartialEq, Debug)]
pub struct Node {
    input: HashMap<Resource, ResourceValue>,
    output: HashMap<Resource, ResourceValue>,
}

impl Node {
    // returns true if the resources in other satisify some need in self
    pub fn can_connect(&self, other: &Node) -> bool {
        for key in self.input.keys() {
            if other.output.contains_key(&key) {
                return true;
            }
        }
        false
    }

    pub fn validate(&self) -> Result<(), SolverError> {
        // Check that there is a time input
        if !self.input.contains_key(&Resource::time()) {
            return Err(SolverError::MalformedNode(String::from("missing time in input")));
        }
        // Check that there is no time in output
        if self.output.contains_key(&Resource::time()) {
            return Err(SolverError::MalformedNode(String::from("output contains time")));
        }
        Ok(())
    }
}

/* ProblemScorer calculates a 'value' for the problem and subproblems, such as
 * total produced isk/hour
 */
pub trait ProblemScorer: Debug {
    fn score(&self, problem: &Problem) -> f64;
}

/* Problem represents a problem we are solving for; it is expected to contain a target
 * node, and will produce sub-problems to fulfill the problem, recursively working up
 * down to base problems which pull from raw resources (items that require no input)
 */
#[derive(Debug)]
pub struct Problem {
    target: Node,
    // Represents the total 'resulting' resources from this task, including
    // any left-over material or spent money
    output: HashMap<Resource, ResourceValue>,
    problems: Vec<Box<Problem>>,
    factory: Box<NodeFactory>,
    scorer: Box<ProblemScorer>,
}

impl Problem {
    pub fn solve(&mut self) {
        let max = None;
        for soln in self.solutions.iter() 
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct Factory {}

    impl Factory {
        fn amarr_shuttle() -> Node {
            Node{
                input: map!(HashMap::default(),
                            Resource::from("Tritanium") => 2778,
                            Resource::time() => {Duration::hours(1) + Duration::minutes(40)},
                ),
                output: map!(HashMap::default(), Resource::from("Amarr Shuttle") => 1),
            }
        }

        fn tritanium() -> Node {
            Node{
                input: map!(HashMap::default(), Resource::time() => Duration::seconds(1)),
                output: map!(HashMap::default(), Resource::from("Tritanium") => 1),
            }
        }
    }

    #[test]
    fn make_a_shuttle() {
        Factory::amarr_shuttle();
    }

    #[test]
    fn can_connect_tritanium_to_amarr_shuttle() {
        let tri = Factory::tritanium();

        let amarr_shuttle = Factory::amarr_shuttle();
        assert_eq!(amarr_shuttle.can_connect(&tri), true);
    }

    #[test]
    fn cant_connect_incompatible_resources() {
        let tri = Factory::tritanium();
        let tri2 = Factory::tritanium();

        assert_eq!(tri.can_connect(&tri2), false);
    }

    #[test]
    fn invalid_node() {
        assert_eq!(Node{
            input: map!(HashMap::default(), Resource::from("Tritanium") => 1),
            output: map!(HashMap::default(), Resource::from("Tritanium") => 1),
        }.validate().is_err(), true);

        assert_eq!(Node{
            input: map!(HashMap::default(), Resource::time()=> 1),
            output: map!(HashMap::default(), Resource::time() => 1),
        }.validate().is_err(), true);
    }
}
